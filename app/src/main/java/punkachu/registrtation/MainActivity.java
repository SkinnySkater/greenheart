package punkachu.registrtation;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.Profile;

import punkachu.registrtation.Connection.activity.HorizontalLitView.CustomRecyclerViewAdapter;
import punkachu.registrtation.Connection.activity.HorizontalLitView.Data;
import punkachu.registrtation.Connection.activity.HorizontalLitView.DividerItemDecoration;
import punkachu.registrtation.Connection.activity.LoginActivity;
import punkachu.registrtation.Connection.activity.helper.CircularListAdapter;
import punkachu.registrtation.Connection.activity.helper.SQLiteHandler;
import punkachu.registrtation.Connection.activity.helper.SessionManager;
import punkachu.registrtation.Connection.activity.helper.ImageHelper;

/**
 * Created by root on 12/18/16.
 */


public class MainActivity extends Activity
{

    private TextView btnLogout;

    private SQLiteHandler db;
    private SessionManager session;

    private ImageView profilePicImageView;
    private Profile profile;

    /* recycler horizontal listview praameters*/
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static String LOG_TAG = "RecyclerViewActivity";



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_main_recyclerview);
        mRecyclerView.setHasFixedSize(true);


        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);//new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        mAdapter = new CustomRecyclerViewAdapter(getDataSet());
        mRecyclerView.setAdapter(mAdapter);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, LinearLayoutManager.HORIZONTAL);
        mRecyclerView.addItemDecoration(itemDecoration);

        //Set the circular list on both side
        mRecyclerView.getLayoutManager().scrollToPosition(Integer.MAX_VALUE / 2);

        btnLogout           = (TextView) findViewById(R.id.btnLogout);
        profilePicImageView = (ImageView) findViewById(R.id.profilPic);

        // SqLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // session manager
        session = new SessionManager(getApplicationContext());

        if (!session.isLoggedIn())
            logoutUser();

        //display user pic
        updateUI();

        // Fetching user details from sqlite
        HashMap<String, String> user = db.getUserDetails();

        System.out.println(user);

        String name  = user.get("name");
        String email = user.get("email");


        // Logout button click event
        btnLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        ((CustomRecyclerViewAdapter) mAdapter).setOnItemClickListener(new CustomRecyclerViewAdapter.MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                Log.i(LOG_TAG, " Clicked on Item " + position);
                System.out.println(" Clicked on Item " + position % 4);
            }
        });
    }

    private ArrayList<Data> getDataSet()
    {
        ArrayList results = new ArrayList<Data>();

        Bitmap icon1 = BitmapFactory.decodeResource(getResources(), R.drawable.pokemon_icon);
        Bitmap icon2 = BitmapFactory.decodeResource(getResources(), R.drawable.earth);
        Bitmap icon3 = BitmapFactory.decodeResource(getResources(), R.drawable.map_icon);
        Bitmap icon4 = BitmapFactory.decodeResource(getResources(), R.drawable.settings);

        Data obj1 = new Data(icon1, "Profile");
        Data obj2 = new Data(icon2, "Event");
        Data obj3 = new Data(icon3, "Map");
        Data obj4 = new Data(icon4, "Option");

        results.add(0, obj1);
        results.add(1, obj2);
        results.add(2, obj3);
        results.add(3, obj4);

        return results;
    }

    private void updateUI()
    {
        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;

        profile = Profile.getCurrentProfile();
        if (enableButtons && profile != null)
        {
            if (new LoadProfileImage(profilePicImageView).execute(profile.getProfilePictureUri(300, 300).toString()).isCancelled())
            {
                Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.pokemon_icon);
                profilePicImageView.setImageBitmap(ImageHelper.getRoundedCornerBitmap1(icon, 50));
            }
        }
        else
        {
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.pokemon_icon);
            profilePicImageView.setImageBitmap(ImageHelper.getRoundedCornerBitmap1(icon, 50));
        }
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     * */
    private void logoutUser()
    {
        session.setLogin(false);
        db.deleteUsers();
        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Background Async task to load user profile picture from url
     * */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap>
    {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... uri)
        {
            String url = uri[0];
            Bitmap mIcon11 = null;
            try
            {
                InputStream in = new java.net.URL(url).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e)
            {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result)
        {
            if (result != null)
                bmImage.setImageBitmap(ImageHelper.getRoundedCornerBitmap1(result, 100));
        }
    }
}