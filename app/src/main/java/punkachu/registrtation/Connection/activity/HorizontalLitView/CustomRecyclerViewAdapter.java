package punkachu.registrtation.Connection.activity.HorizontalLitView;

/**
 * Created by root on 1/23/17.
 */

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import punkachu.registrtation.R;

public class CustomRecyclerViewAdapter extends RecyclerView.Adapter<CustomRecyclerViewAdapter.DataObjectHolder>
{
    private static String LOG_TAG = "MyRecyclerViewAdapter";
    private static int LIST_SIZE = 4;
    private ArrayList<Data> mDataset;
    private static MyClickListener myClickListener;

    public static class DataObjectHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ImageView img;
        TextView label;

        public DataObjectHolder(View itemView)
        {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.label_list);
            img = (ImageView) itemView.findViewById(R.id.image_list);
            Log.i(LOG_TAG, "Adding Listener");
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getPosition(), v);
        }
    }

    public void setOnItemClickListener(MyClickListener myClickListener)
    {
        this.myClickListener = myClickListener;
    }

    public CustomRecyclerViewAdapter(ArrayList<Data> myDataset)
    {
        mDataset = myDataset;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }



    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        int positionInList = position % LIST_SIZE;
        holder.label.setText(mDataset.get(positionInList).getmText());
        holder.img.setImageBitmap(mDataset.get(positionInList).getmIcon());
    }

    public void addItem(Data dataObj, int index)
    {
        mDataset.add(dataObj);
        notifyItemInserted(index);
    }

    public void deleteItem(int index)
    {
        mDataset.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public int getItemCount()
    {
        return Integer.MAX_VALUE;
    }

    public interface MyClickListener
    {
        public void onItemClick(int position, View v);
    }
}