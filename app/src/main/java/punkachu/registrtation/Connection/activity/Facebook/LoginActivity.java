package punkachu.registrtation.Connection.activity.Facebook;

/**
 * Created by root on 12/19/16.
 */

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import com.facebook.Profile;

import punkachu.registrtation.Connection.activity.helper.SessionManager;
import punkachu.registrtation.MainActivity;
import punkachu.registrtation.R;

public class LoginActivity extends AppCompatActivity
{
    private static final String TAG = punkachu.registrtation.Connection.activity.Facebook.LoginActivity.class.getSimpleName();

    private TextView InfoUser;
    private SessionManager session;
    private Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Making notification bar transparent
        /*if (Build.VERSION.SDK_INT >= 21)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);*/

        setContentView(R.layout.fb_activity);

        InfoUser = (TextView) findViewById(R.id.InfoUser);
        // Session manager
        session = new SessionManager(getApplicationContext());
        profile = Profile.getCurrentProfile();

        if (session.isLoggedIn())
        {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        if (fragment == null)
        {
            fragment = new FacebookFragment();
            fm.beginTransaction().add(R.id.fragment_container, fragment).commit();
        }
    }
}