package punkachu.registrtation.Connection.activity.HorizontalLitView;

import android.graphics.Bitmap;

/**
 * Created by root on 1/24/17.
 */

public class Data
{
    private Bitmap mIcon;
    private String mText;

    public Data(Bitmap mIcon, String text2)
    {
        this.mIcon = mIcon;
        this.mText = text2;
    }

    public Bitmap getmIcon() {
        return mIcon;
    }

    public void setmIcone(Bitmap icone) {
        this.mIcon = icone;
    }

    public String getmText() {
        return mText;
    }

    public void setmText(String mText) {
        this.mText = mText;
    }
}