package punkachu.registrtation.Connection.activity;

/**
 * Created by root on 12/18/16.
 */
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import punkachu.registrtation.GlobalHelper.MyBounceInterpolator;
import punkachu.registrtation.app.PrefManager;
import punkachu.registrtation.MainActivity;
import punkachu.registrtation.R;
import punkachu.registrtation.app.AppConfig;
import punkachu.registrtation.app.AppController;
import punkachu.registrtation.Connection.activity.helper.SQLiteHandler;
import punkachu.registrtation.Connection.activity.helper.SessionManager;

import static com.facebook.FacebookSdk.getApplicationContext;


public class LoginActivity extends AppCompatActivity
{
    private static final String TAG = punkachu.registrtation.Connection.activity.LoginActivity.class.getSimpleName();

    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    public static CallbackManager callbackManager;
    private Profile profile;

    private ImageButton imgButtonFb;
    private ImageButton imgButtonGoogle;
    private ImageButton imgButtonTwiter;

    private PrefManager prefManager;

    private String fontPath;

    private TextView brand;
    private Animation anim;
    private Animation heart;
    private ImageView brandImg;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        FacebookSdk.sdkInitialize(getApplicationContext());

        final punkachu.registrtation.Connection.activity.LoginActivity activity = this;

        fontPath = "fonts/Greenman.ttf";
        brand = (TextView) findViewById(R.id.BrandTextLogin);
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        brand.setTypeface(tf);

        //SET ANIMATION

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn())
        {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        // Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);

        // RUN ANIM BUTTON
        brandImg = (ImageView) findViewById(R.id.BrandImageView);
        HeartBleed();
        /*heart = AnimationUtils.loadAnimation(this, R.anim.heartbit);
        brandImg.startAnimation(heart);*/
        buttonAnimFB(R.id.FbimageButtonLog, 800);
        buttonAnimT(R.id.TwitimageButtonLog, 800);
        buttonAnimGG(R.id.GoogleimageButtonLog, 800);


        // Facebook image Button to link the facebook log in
        imgButtonFb.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //if facebook sdk has already registered the current user just retrieve it & log in
                callbackManager = CallbackManager.Factory.create();
                // loginButton.setReadPermissions(Arrays.asList("user_status"));

                imgButtonFb.clearAnimation();

                didTapFbButton(v);

                if (prefManager.isFirstTimeLaunch()) {
                    runRegistration();
                }

                // Set permissions
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this,
                        Arrays.asList("public_profile", "email", "user_friends"));

                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>()
                        {
                            @Override
                            public void onSuccess(LoginResult loginResult)
                            {
                                System.out.println("Success");
                                GraphRequest request = GraphRequest.newMeRequest(
                                        loginResult.getAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback()
                                        {
                                            @Override
                                            public void onCompleted(JSONObject json, GraphResponse response)
                                            {
                                                if (response.getError() != null)
                                                    // handle error
                                                    System.out.println("ERROR");
                                                else
                                                {
                                                    System.out.println("Success");
                                                    try
                                                    {
                                                        String jsonresult = String.valueOf(json);
                                                        String str_email = json.getString("email");
                                                        String str_id = json.getString("id");
                                                        checkLogin(str_email, str_id);
                                                    } catch (JSONException e) {e.printStackTrace();}
                                                }
                                            }
                                        });
                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,name,link,email,first_name,last_name,gender");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }

                            @Override
                            public void onCancel() {Log.d("TAG_CANCEL", "On cancel");}

                            @Override
                            public void onError(FacebookException error) {Log.d("TAG_ERROR", error.toString());}
                        });
            }
        });

    }


    /* DEFINE ANIM METHODES */
    private void buttonAnimFB(int buttonId, long duration)
    {
        imgButtonFb    = (ImageButton) findViewById(R.id.FbimageButtonLog);
        anim = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fallen);
        anim.setDuration(duration);
        imgButtonFb.startAnimation(anim);
    }

    private void buttonAnimGG(int buttonId, long duration)
    {
        imgButtonGoogle  = (ImageButton) findViewById(R.id.GoogleimageButtonLog);
        anim             = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fallen);
        anim.setDuration(duration);
        anim.setStartOffset(duration / 2);
        imgButtonGoogle.startAnimation(anim);
    }

    private void buttonAnimT(int buttonId, long duration)
    {
        imgButtonTwiter  = (ImageButton) findViewById(R.id.TwitimageButtonLog);
        anim             = AnimationUtils.loadAnimation(LoginActivity.this, R.anim.fallen);
        anim.setDuration(duration);
        anim.setStartOffset(duration / 2);
        imgButtonTwiter.startAnimation(anim);
    }

    public void didTapFbButton(View view)
    {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        imgButtonFb.startAnimation(myAnim);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        imgButtonFb.startAnimation(myAnim);
    }

    public void HeartBleed()
    {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        brandImg.startAnimation(myAnim);
        // Use bounce interpolator with amplitude 0.2 and frequency 20
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);

        brandImg.startAnimation(myAnim);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }


    /**
     * if first launch app, then run registration activity
     * */
    private void runRegistration()
    {
        prefManager.setFirstTimeLaunch(false);
        Intent i = new Intent(getApplicationContext()
                , punkachu.registrtation.Connection.activity.Facebook.LoginActivity.class);
        startActivityForResult(i, 2);
        finish();
    }
    /**
     * function to verify login details in mysql db
     * */
    private void checkLogin(final String email, final String password)
    {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Authenticating...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>()
        {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("name");
                        String email = user.getString("email");
                        String created_at = user.getString("created_at");

                        // Inserting row in users table
                        db.addUser(name, email, uid, created_at);

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams()
            {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog()
    {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog()
    {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}