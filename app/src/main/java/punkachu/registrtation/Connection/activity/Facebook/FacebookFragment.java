package punkachu.registrtation.Connection.activity.Facebook;

/**
 * Created by root on 12/19/16.
 */


import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


import punkachu.registrtation.Connection.activity.helper.ImageHelper;
import punkachu.registrtation.Connection.activity.helper.JSONParser;
import punkachu.registrtation.app.PrefManager;
import punkachu.registrtation.Connection.activity.helper.SQLiteHandler;
import punkachu.registrtation.Connection.activity.helper.SessionManager;
import punkachu.registrtation.MainActivity;
import punkachu.registrtation.R;
import punkachu.registrtation.app.AppConfig;
import punkachu.registrtation.app.AppController;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FacebookFragment extends Fragment
{
    private static final String TAG = FacebookFragment.class.getSimpleName();

    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    private Animation animFadeout;
    private Animation animMove;
    private Animation animZoomout;

    private Profile profile;

    private LoginButton loginButton;

    private final String PENDING_ACTION_BUNDLE_KEY = "com.example.hellofacebook:PendingAction";

    private ImageView profilePicImageView;
    private TextView greeting;
    private PendingAction pendingAction = PendingAction.NONE;

    private boolean canPresentShareDialog;
    private PrefManager prefManager;

    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private ShareDialog shareDialog;
    private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>()
    {
        @Override
        public void onCancel() {
            Log.d("FacebookFragment", "Canceled");
        }

        @Override
        public void onError(FacebookException error)
        {
            Log.d("FacebookFragment", String.format("Error: %s", error.toString()));
            String title = getString(R.string.error);
            String alertMessage = error.getMessage();
            showResult(title, alertMessage);
        }

        @Override
        public void onSuccess(Sharer.Result result)
        {
            Log.d("FacebookFragment", "Success!");
        }

        private void showResult(String title, String alertMessage)
        {
            new AlertDialog.Builder(getActivity())
                    .setTitle(title)
                    .setMessage(alertMessage)
                    .setPositiveButton(R.string.ok, null)
                    .show();
        }
    };

    private enum PendingAction
    {
        NONE
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity());
        // Other app specific specialization
        prefManager = new PrefManager(this.getActivity());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_facebook, parent, false);

        // Progress dialog
        pDialog = new ProgressDialog(this.getActivity());
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        loginButton = (LoginButton) v.findViewById(R.id.loginButton);

        animFadeout = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        animMove = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move);
        animZoomout = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);

        //Request authorization about profile info
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_friends"));

        // If using in a fragment
        loginButton.setFragment(this);
        callbackManager = CallbackManager.Factory.create();

        // Callback registration --> ADD EXRA INFORMATIONS HERE
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>()
        {
            @Override
            public void onSuccess(LoginResult loginResult)
            {
                Toast toast = Toast.makeText(getActivity(), "Logged In", Toast.LENGTH_SHORT);

                // Facebook Email address
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response)
                            {
                                Log.v("LoginActivity Response ", response.toString());
                                Log.d("Profile", profile.toString());
                                Log.d("AdoptMySong", object.toString());
                                final String name = JSONParser.getName(object);
                                // Application code
                                try
                                {
                                    final String FEmail = object.getString("email");
                                    Log.v("Email = ", " " + FEmail);

                                    //send data to database
                                    loginButton.startAnimation(animMove);
                                    loginButton.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            loginButton.setVisibility(loginButton.GONE);
                                            greeting.setVisibility(View.GONE);
                                            loginButton.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    profilePicImageView.startAnimation(animZoomout);
                                                }
                                            }, 700);

                                        }
                                    }, 1700);
                                    registerUser(name, FEmail, profile.getId());
                                } catch (JSONException e) { e.printStackTrace(); }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();

                toast.show();
                updateUI();
            }

            @Override
            public void onCancel()
            {
                // App code
                if (pendingAction != PendingAction.NONE)
                {
                    showAlert();
                    pendingAction = PendingAction.NONE;
                }
                updateUI();
            }

            @Override
            public void onError(FacebookException exception)
            {
                if (pendingAction != PendingAction.NONE && exception instanceof FacebookAuthorizationException)
                {
                    showAlert();
                    pendingAction = PendingAction.NONE;
                }
                updateUI();

            }

            private void showAlert()
            {
                new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.cancelled)
                        .setMessage(R.string.permission_not_granted)
                        .setPositiveButton(R.string.ok, null)
                        .show();
            }

        });


        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, shareCallback);

        if (savedInstanceState != null)
        {
            String name = savedInstanceState.getString(PENDING_ACTION_BUNDLE_KEY);
            pendingAction = PendingAction.valueOf(name);
        }

        profileTracker = new ProfileTracker()
        {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile)
            {
                updateUI();
            }
        };

        profilePicImageView = (ImageView) v.findViewById(R.id.profilePicture);
        greeting = (TextView) v.findViewById(R.id.greeting);


        // Can we present the share dialog for regular links?
        canPresentShareDialog = ShareDialog.canShow(ShareLinkContent.class);

        return v;
    }

    @Override
    public void onResume()
    {
        super.onResume();

        // Call the 'activateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onResume methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.activateApp(getActivity());
        updateUI();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        // Call the 'deactivateApp' method to log an app event for use in analytics and advertising
        // reporting.  Do so in the onPause methods of the primary Activities that an app may be
        // launched into.
        AppEventsLogger.deactivateApp(getActivity());
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        profileTracker.stopTracking();
    }



    private void updateUI()
    {
        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;

        profile = Profile.getCurrentProfile();
        if (enableButtons && profile != null)
        {
            new LoadProfileImage(profilePicImageView).execute(profile.getProfilePictureUri(300, 300).toString());
            greeting.setText(getString(R.string.hello_user, profile.getFirstName() + " " + profile.getLastName()));
        }
        else
        {
            Bitmap icon = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.heart);
            profilePicImageView.setImageBitmap(ImageHelper.getRoundedCornerBitmap1(icon, 0));
            greeting.setText(null);
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);

        outState.putString(PENDING_ACTION_BUNDLE_KEY, pendingAction.name());
    }


    /**
     * Background Async task to load user profile picture from url
     * */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap>
    {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... uri)
        {
            String url = uri[0];
            Bitmap mIcon11 = null;
            try
            {
                InputStream in = new java.net.URL(url).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e)
            {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result)
        {
            if (result != null)
                bmImage.setImageBitmap(ImageHelper.getRoundedCornerBitmap1(result, 50));
        }
    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to register url
     * */
    private void registerUser(final String name, final String email, final String password)
    {
        // Tag used to cancel the request
        String tag_string_req = "req_register";

        pDialog.setMessage("Registering ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_REGISTER, new Response.Listener<String>()
        {

            @Override
            public void onResponse(String response)
            {
                Log.d(TAG, "Register Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    if (!error) {
                        // User successfully stored in MySQL
                        // Now store the user in sqlite
                        String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("name");
                        final String email = user.getString("email");
                        String created_at = user.getString("created_at");

                        // Inserting row in users table
                        db.addUser(name, email, uid, created_at);

                        Toast.makeText(getApplicationContext()
                                , "User successfully registered. Entering..!"
                                , Toast.LENGTH_LONG).show();

                        loginButton.startAnimation(animMove);
                        loginButton.postDelayed(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                loginButton.setVisibility(loginButton.GONE);
                                greeting.setVisibility(View.GONE);
                                profilePicImageView.startAnimation(animZoomout);
                                loginButton.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //SUCCESSFL so set the prefmanagr bool to false
                                        prefManager.setFirstTimeLaunch(false);
                                        //send email and name to the login class
                                        /*Intent intent = new Intent(getActivity(), punkachu.registrtation.Connection.activity.LoginActivity.class);
                                        String[] extra_data = new String[] {email, profile.getId()};
                                        getActivity().setResult(RESULT_OK, intent);
                                        intent.putExtra("EXTRA_SESSION_DATA", extra_data);
                                        getActivity().setResult(2, intent);
                                        getActivity().startActivityForResult(intent, 2);*/
                                        checkLogin(email, profile.getId());
                                    }
                                }, 900);
                            }
                        }, 2000);
                    }
                    else
                    {

                        // Error occurred in registration. Get the error
                        // message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                        //Then return to the main login page
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run()
                            {
                                Intent intent = new Intent(getActivity(), punkachu.registrtation.Connection.activity.LoginActivity.class);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                            }
                        }, 3000);
                    }
                } catch (JSONException e) {e.printStackTrace();}

            }
        }, new Response.ErrorListener()
        {

            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.e(TAG, "Registration Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        })
        {

            @Override
            protected Map<String, String> getParams()
            {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("name", name);
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog()
    {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog()
    {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * function to verify login details in mysql db
     * */
    private void checkLogin(final String email, final String password)
    {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Authenticating...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, AppConfig.URL_LOGIN, new Response.Listener<String>()
        {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("name");
                        String email = user.getString("email");
                        String created_at = user.getString("created_at");

                        // Inserting row in users table
                        db.addUser(name, email, uid, created_at);

                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams()
            {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

}
