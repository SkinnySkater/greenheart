package punkachu.registrtation.GlobalHelper;

/**
 * Created by root on 2/20/17.
 */

public class MyBounceInterpolator implements android.view.animation.Interpolator
{
    double mAmplitude = 1;
    double mFrequency = 10;

    public MyBounceInterpolator(double amplitude, double frequency) {
        mAmplitude = amplitude;
        mFrequency = frequency;
    }

    public float getInterpolation(float time) {
        return (float) (-1 * Math.pow(Math.E, -time/ mAmplitude) *
                Math.cos(mFrequency * time) + 1);
    }

}